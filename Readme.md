# Graphql Socketio Subscriptions

A graphql subscriptions implementation using Socket.IO and apollo's graphql-subscriptions.

Used by Pro dashboard server.

### Package manager

Yarn

### Dependencies

Resolutions are set to fix vulnerabilities in (sub) dependencies.

- `minimist`
- `minimatch`
- `path-parse`

Check for vulnerabilities with `yarn audit`.

### Test

Run `yarn test`

### Commit

Run `yarn build` before pushing.