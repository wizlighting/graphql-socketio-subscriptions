/// <reference types="socket.io-client" />
import { PubSubEngine } from "graphql-subscriptions";
export interface SocketIOPubSubOptions {
    url?: string;
    connectionOptions?: SocketIOClient.ConnectOpts;
    connectionListener?: (evt: any) => void;
    messageTransform: (event: string, data: string) => ParsedMessage;
    triggerNameTransform?: (trigger: string) => string;
    eventList: Array<string>;
    publisher?: SocketIOClient.Socket;
    subscriber?: SocketIOClient.Socket;
}
export type ParsedMessage = {
    trigger: string;
    event: string;
    data: object;
};
export declare class SocketIOPubSub implements PubSubEngine {
    constructor(options: SocketIOPubSubOptions);
    publish(event: string, payload: any): Promise<void>;
    subscribe(trigger: string, onMessage: Function, options?: Object): Promise<number>;
    unsubscribe(subId: number): any;
    asyncIterator<T>(triggers: string | string[]): AsyncIterator<T>;
    get subscriber(): SocketIOClient.Socket;
    get publisher(): SocketIOClient.Socket;
    disconnect(): void;
    notifySubscribers(event: string, data: string): void;
    private socketIOSubscriber;
    private socketIOPublisher;
    private subscriptionMap;
    private subscriptionRefs;
    private currentSubscriptionId;
    private messageTransform;
    private triggerNameTransform;
    private eventList;
}
