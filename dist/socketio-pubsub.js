"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketIOPubSub = void 0;
const io = require("socket.io-client");
const pubsub_async_iterator_1 = require("./pubsub-async-iterator");
class SocketIOPubSub {
    constructor(options) {
        const { url, connectionOptions, publisher, connectionListener, subscriber, messageTransform, triggerNameTransform, eventList } = options;
        if (publisher && subscriber) {
            this.socketIOPublisher = publisher;
            this.socketIOSubscriber = subscriber;
        }
        else {
            this.socketIOPublisher = io(url, connectionOptions);
            this.socketIOSubscriber = io(url, connectionOptions);
            if (connectionListener) {
                this.socketIOPublisher.on("connect", () => connectionListener("Connected"));
                this.socketIOPublisher.on("error", (err) => connectionListener(`Publisher error - ${err}`));
                this.socketIOSubscriber.on("connect", () => connectionListener("Connected"));
                this.socketIOSubscriber.on("error", (err) => connectionListener(`Subscriber error - ${err}`));
            }
            else {
                this.socketIOPublisher.on("error", console.error);
                this.socketIOSubscriber.on("error", console.error);
            }
        }
        this.subscriptionMap = {};
        this.subscriptionRefs = {};
        this.currentSubscriptionId = 0;
        this.messageTransform = messageTransform;
        // subscribe to events
        for (const event of eventList) {
            // on incoming message
            this.socketIOSubscriber.on(event, (data) => {
                this.notifySubscribers(event, data);
            });
        }
        this.eventList = eventList;
        this.triggerNameTransform =
            triggerNameTransform || ((trigger) => trigger);
    }
    publish(event, payload) {
        const msgData = this.messageTransform(event, payload);
        this.socketIOPublisher.emit("join", this.triggerNameTransform(msgData.trigger));
        this.socketIOPublisher.emit(msgData.event, msgData.data);
        this.socketIOPublisher.emit("leave", this.triggerNameTransform(msgData.trigger));
        return Promise.resolve();
    }
    subscribe(trigger, onMessage, options) {
        return __awaiter(this, void 0, void 0, function* () {
            // create and save subscription id
            this.currentSubscriptionId = this.currentSubscriptionId + 1;
            const subId = this.currentSubscriptionId;
            this.subscriptionMap[this.currentSubscriptionId] = { trigger, onMessage };
            // check if we've already subscribed
            if (this.subscriptionRefs[trigger] == undefined) {
                this.subscriptionRefs[trigger] = [subId];
                this.socketIOSubscriber.emit("join", this.triggerNameTransform(trigger));
            }
            else {
                this.subscriptionRefs[trigger] = [
                    ...this.subscriptionRefs[trigger],
                    subId
                ];
            }
            return subId;
        });
    }
    unsubscribe(subId) {
        const { trigger, onMessage } = this.subscriptionMap[subId];
        if (trigger == undefined) {
            return;
        }
        const refs = this.subscriptionRefs[trigger];
        if (refs && refs.length <= 1) {
            delete this.subscriptionMap[subId];
            delete this.subscriptionRefs[trigger];
            this.socketIOSubscriber.emit("leave", trigger);
        }
        else {
            this.subscriptionRefs[trigger] = refs.filter(el => el !== subId);
        }
    }
    asyncIterator(triggers) {
        return new pubsub_async_iterator_1.PubSubAsyncIterator(this, triggers);
    }
    get subscriber() {
        return this.socketIOSubscriber;
    }
    get publisher() {
        return this.socketIOPublisher;
    }
    disconnect() {
        this.socketIOPublisher.close();
        this.socketIOSubscriber.close();
    }
    notifySubscribers(event, data) {
        // transform it to extract delimiter data
        const msg = this.messageTransform(event, data);
        // and notify subscribers
        const subscribers = this.subscriptionRefs[msg.trigger];
        if (subscribers == undefined) {
            return;
        }
        for (const subscriber of subscribers) {
            this.subscriptionMap[subscriber].onMessage(msg);
        }
    }
}
exports.SocketIOPubSub = SocketIOPubSub;
//# sourceMappingURL=socketio-pubsub.js.map