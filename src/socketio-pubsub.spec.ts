import {} from "mocha";
import { expect } from "chai";
import * as sinon from "sinon";
import { SocketIOPubSub } from "./socketio-pubsub";
import * as io from "socket.io-client";

const messageTransform = (event: string, data: string) => ({
  trigger: event,
  event,
  data: JSON.parse(data)
});

const eventList = ["event1", "event2"];

const _self: { [key:string]: any } = {};

afterEach(() => {
  _self.pubsub.disconnect();
});

describe("Creating pubsub", () => {
  it("should use socket.io clients if passed", () => {
    const subscriber = io("http://localhost");
    const publisher = io("http://localhost");
    const opts = {
      publisher,
      subscriber,
      eventList,
      messageTransform
    };
    const pubsub = new SocketIOPubSub(opts);
    expect(pubsub.publisher).to.eq(publisher);
    expect(pubsub.subscriber).to.eq(subscriber);
    _self.pubsub = pubsub;
  });
  it("should create new socket.io clients if nothing was passed", () => {
    const opts = {
      url: "http://localhost",
      eventList,
      messageTransform,
      connectionOptions: {}
    };
    const pubsub = new SocketIOPubSub(opts);
    expect(pubsub.publisher).to.be.an.instanceof(io.Socket);
    expect(pubsub.subscriber).to.be.an.instanceof(io.Socket);
    _self.pubsub = pubsub;
  });
});

describe("Subscribing", () => {
  beforeEach(() => {
    const subscriber = io("http://localhost");
    _self.subscriber = subscriber;
    _self.subscriberEmitSpy = sinon.spy(_self.subscriber, "emit");

    const publisher = io("http://localhost");
    _self.opts = {
      publisher,
      subscriber,
      eventList,
      messageTransform,
    };
    _self.listener = sinon.spy();
  });

  it("should create `on` callbacks for event list", async () => {
    const spy = sinon.spy(_self.subscriber, "on");

    const pubsub = new SocketIOPubSub(_self.opts);
    const result1 = await pubsub.subscribe("test1", _self.listener);
    expect(eventList.map((event: string) => spy.withArgs(event).calledOnce))
      .to.be.an("array")
      .lengthOf(eventList.length)
      .that.does.not.include(false);
    _self.pubsub = pubsub;
  });

  it("should subscribe to passed topics", async () => {
    const pubsub = new SocketIOPubSub(_self.opts);
    const result1 = await pubsub.subscribe("test1", _self.listener);
    const result2 = await pubsub.subscribe("test2", _self.listener);
    const result3 = await pubsub.subscribe("test3", _self.listener);

    expect(result1).to.not.eq(0);
    expect(
      _self.subscriberEmitSpy.calledThrice,
      "join was emitted less than 3 times for 3 different triggers"
    ).to.be.true;
    _self.pubsub = pubsub;
  });

  it("should subscribe to the same topic only once", async () => {
    const pubsub = new SocketIOPubSub(_self.opts);
    const result1 = await pubsub.subscribe("test", _self.listener);
    const result2 = await pubsub.subscribe("test", _self.listener);
    const result3 = await pubsub.subscribe("test", _self.listener);

    expect(result1).to.not.eq(0);
    expect(
      _self.subscriberEmitSpy.calledOnce,
      "join was emitted more than once for the same trigger"
    ).to.be.true;
    _self.pubsub = pubsub;
  });

  it("should transform trigger name before subscribing", async () => {
    _self.opts.triggerNameTransform = (trigger: string) => {
      return "prefix" + trigger;
    };
    const pubsub = new SocketIOPubSub(_self.opts);
    const result1 = await pubsub.subscribe("test", _self.listener);
    expect(_self.subscriberEmitSpy.calledOnceWith("join", "prefixtest")).to.be
      .true;
    _self.pubsub = pubsub;
  });

  it("should call `on` callbacks for event list on incoming messages", async () => {
    // create a stub for socketIO.on function
    const call = (event: string, callback: ((data: string) => void)) => {
      return (dt: string) => {
        callback(dt);
      };
    };

    // in the end it will pretend to be valid call from socketIO lib
    let eventHandler: (data: string) => void;

    const onStub = sinon
      .stub(_self.subscriber, "on")
      .callsFake(function (
        event: string,
        callback: (data: string) => void
      ): void {
        eventHandler = call(event, callback);
      });

    _self.opts.subscriber = _self.subscriber;
    _self.opts.messageTransform = (event: string, data: string) => ({
      trigger: JSON.parse(data).trigger,
      event,
      data: JSON.parse(data).data
    });

    // create 4 spies, and bunch of messages
    // l1 and l3 are subscribed to the same trigger and are supposed to be called twice
    // l2 – receives one event and will be called once
    // l4 – receives no events
    const l1 = sinon.spy();
    const l2 = sinon.spy();
    const l3 = sinon.spy();
    const l4 = sinon.spy();
    const pubsub = new SocketIOPubSub(_self.opts);
    const result1 = await pubsub.subscribe("test", l1);
    const result2 = await pubsub.subscribe("test2", l2);
    const result3 = await pubsub.subscribe("test", l3);
    const result4 = await pubsub.subscribe("test4", l3);

    eventHandler!('{"trigger":"test", "data":123}');
    eventHandler!('{"trigger":"test2", "data":123}');
    eventHandler!('{"trigger":"test", "data":234}');
    // adding the case when received the message, nobody listens for
    // – lib should do nothing this case
    eventHandler!('{"trigger":"test5", "data":234}');

    expect(l1.calledTwice, "listener was not called twice for 2 messages").to.be
      .true;
    expect(
      l2.calledOnce,
      "listener was not called exactly once for the listener"
    ).to.be.true;
    expect(
      l3.calledTwice,
      "another listener for an existing trigger was not called twice"
    ).to.be.true;
    expect(l4.notCalled, "listener was called but event was not sent");

    _self.pubsub = pubsub;
  });
});

describe("Unsubscribing", () => {
  beforeEach(() => {
    const subscriber = io("http://localhost");
    _self.subscriber = subscriber;
    _self.subscriberEmitSpy = sinon.spy(_self.subscriber, "emit");

    const publisher = io("http://localhost");
    _self.opts = {
      publisher,
      subscriber,
      eventList,
      messageTransform,
    };
  });
  it("should not leave the room if there are some listeners left", async () => {
    const pubsub = new SocketIOPubSub(_self.opts);
    const result1 = await pubsub.subscribe("test", _self.listener);
    const result2 = await pubsub.subscribe("test", _self.listener);
    const result3 = await pubsub.subscribe("test", _self.listener);

    pubsub.unsubscribe(result1);
    expect(
      _self.subscriberEmitSpy.neverCalledWith("leave"),
      "room was left when there were some listeners still present"
    ).to.be.true;
    _self.pubsub = pubsub;
  });

  it("should leave the room if there are no listeners left", async () => {
    const pubsub = new SocketIOPubSub(_self.opts);
    const result1 = await pubsub.subscribe("test", _self.listener);
    const result2 = await pubsub.subscribe("test", _self.listener);
    const result3 = await pubsub.subscribe("test", _self.listener);

    pubsub.unsubscribe(result1);
    pubsub.unsubscribe(result2);
    pubsub.unsubscribe(result3);

    expect(
      _self.subscriberEmitSpy.calledWithExactly("leave", "test"),
      "room was left when there were some listeners still present"
    ).to.be.true;
    _self.pubsub = pubsub;
  });
});

describe("Publishing", () => {
  beforeEach(() => {
    const subscriber = io("http://localhost");
    const publisher = io("http://localhost");
    _self.publisherEmitSpy = sinon.spy(publisher, "emit");
    _self.opts = {
      publisher,
      subscriber,
      eventList,
      messageTransform
    };
  });

  it("should publish a message", async () => {
    const pubsub = new SocketIOPubSub(_self.opts);
    const stringPayload = JSON.stringify({ a: 123 });
    pubsub.publish("test", stringPayload);

    expect(_self.publisherEmitSpy.calledOnceWith("test", stringPayload));
    _self.pubsub = pubsub;
  });
});
