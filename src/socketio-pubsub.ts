import { PubSubEngine } from "graphql-subscriptions";
import * as io from "socket.io-client";
import { PubSubAsyncIterator } from "./pubsub-async-iterator";

export interface SocketIOPubSubOptions {
  url?: string;
  connectionOptions?: SocketIOClient.ConnectOpts;
  connectionListener?: (evt: any) => void;
  messageTransform: (event: string, data: string) => ParsedMessage;
  triggerNameTransform?: (trigger: string) => string;
  eventList: Array<string>;
  publisher?: SocketIOClient.Socket;
  subscriber?: SocketIOClient.Socket;
}
type SubscriptionMap = {
  [subId: number]: { trigger: string; onMessage: Function };
};
type SubscriptionRefsMap = { [trigger: string]: Array<number> };

export type ParsedMessage = {
  trigger: string;
  event: string;
  data: object;
};

export class SocketIOPubSub implements PubSubEngine {
  constructor(options: SocketIOPubSubOptions) {
    const {
      url,
      connectionOptions,
      publisher,
      connectionListener,
      subscriber,
      messageTransform,
      triggerNameTransform,
      eventList
    } = options;
    if (publisher && subscriber) {
      this.socketIOPublisher = publisher;
      this.socketIOSubscriber = subscriber;
    } else {
      this.socketIOPublisher = io(url!, connectionOptions);
      this.socketIOSubscriber = io(url!, connectionOptions);

      if (connectionListener) {
        this.socketIOPublisher.on("connect", () =>
          connectionListener("Connected")
        );
        this.socketIOPublisher.on("error", (err: any) =>
          connectionListener(`Publisher error - ${err}`)
        );
        this.socketIOSubscriber.on("connect", () =>
          connectionListener("Connected")
        );
        this.socketIOSubscriber.on("error", (err: any) =>
          connectionListener(`Subscriber error - ${err}`)
        );
      } else {
        this.socketIOPublisher.on("error", console.error);
        this.socketIOSubscriber.on("error", console.error);
      }
    }

    this.subscriptionMap = {};
    this.subscriptionRefs = {};
    this.currentSubscriptionId = 0;
    this.messageTransform = messageTransform;
    // subscribe to events
    for (const event of eventList) {
      // on incoming message
      this.socketIOSubscriber.on(event, (data: string) => {
        this.notifySubscribers(event, data);
      });
    }
    this.eventList = eventList;
    this.triggerNameTransform =
      triggerNameTransform || ((trigger: string) => trigger);
  }

  public publish(event: string, payload: any): Promise<void> {
    const msgData = this.messageTransform(event, payload);
    this.socketIOPublisher.emit(
      "join",
      this.triggerNameTransform(msgData.trigger)
    );
    this.socketIOPublisher.emit(msgData.event, msgData.data);
    this.socketIOPublisher.emit(
      "leave",
      this.triggerNameTransform(msgData.trigger)
    );
    return Promise.resolve();
  }

  public async subscribe(
    trigger: string,
    onMessage: Function,
    options?: Object
  ): Promise<number> {
    // create and save subscription id
    this.currentSubscriptionId = this.currentSubscriptionId + 1;
    const subId = this.currentSubscriptionId;
    this.subscriptionMap[this.currentSubscriptionId] = { trigger, onMessage };

    // check if we've already subscribed
    if (this.subscriptionRefs[trigger] == undefined) {
      this.subscriptionRefs[trigger] = [subId];
      this.socketIOSubscriber.emit("join", this.triggerNameTransform(trigger));
    } else {
      this.subscriptionRefs[trigger] = [
        ...this.subscriptionRefs[trigger],
        subId
      ];
    }

    return subId;
  }

  unsubscribe(subId: number): any {
    const { trigger, onMessage } = this.subscriptionMap[subId];
    if (trigger == undefined) {
      return;
    }
    const refs = this.subscriptionRefs[trigger];
    if (refs && refs.length <= 1) {
      delete this.subscriptionMap[subId];
      delete this.subscriptionRefs[trigger];
      this.socketIOSubscriber.emit("leave", trigger);
    } else {
      this.subscriptionRefs[trigger] = refs.filter(el => el !== subId);
    }
  }

  asyncIterator<T>(triggers: string | string[]): AsyncIterator<T> {
    return new PubSubAsyncIterator<T>(this, triggers);
  }

  get subscriber(): SocketIOClient.Socket {
    return this.socketIOSubscriber;
  }

  get publisher(): SocketIOClient.Socket {
    return this.socketIOPublisher;
  }

  public disconnect() {
    this.socketIOPublisher.close();
    this.socketIOSubscriber.close();
  }

  public notifySubscribers(event: string, data: string) {
    // transform it to extract delimiter data
    const msg = this.messageTransform(event, data);
    // and notify subscribers
    const subscribers = this.subscriptionRefs[msg.trigger];
    if (subscribers == undefined) {
      return;
    }
    for (const subscriber of subscribers) {
      this.subscriptionMap[subscriber].onMessage(msg);
    }
  }

  private socketIOSubscriber: SocketIOClient.Socket;
  private socketIOPublisher: SocketIOClient.Socket;

  private subscriptionMap: SubscriptionMap;
  private subscriptionRefs: SubscriptionRefsMap;
  private currentSubscriptionId: number;
  private messageTransform: (event: string, data: string) => ParsedMessage;
  private triggerNameTransform: (trigger: string) => string;
  private eventList: Array<string>;
}
