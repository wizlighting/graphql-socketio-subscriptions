import * as io from "socket.io-client";

const socket = io("https://ws-dev.wiz.world");

socket.on("connect", () => {
  console.log("Connected");
});

socket.on("disconnect", () => {
  console.log("Disconnected");
});

socket.on("create/room", (data: any) => {
  console.log(`New event ${data}`);
});

socket.emit("join", "dev/1055");
